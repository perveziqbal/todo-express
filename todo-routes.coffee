#GET     /                 ->  index
#GET     /new              ->  new
#POST    /                 ->  create
#GET     /:id              ->  show
#GET     /:id/edit         ->  edit
#PUT     /:id              ->  update
#DELETE  /:id              ->  destroy
#

todo = require './todo'

_index = (req, res) ->
  res.render 'index', todos : todo.all()

_new = (req, res) ->
  res.render 'new'

_create = (req, res) ->
  (todo.insert (req.param 'title'), (req.param 'completed') == 'yes')
  .done (t) ->
    res.redirect '/todos/' + t.id

_show = (req, res) ->
  res.render 'show', (todo.find req.param 'todo')

_edit = (req, res) ->
  res.render 'edit', (todo.find req.param 'todo')

_update = (req, res) -> # POST
  (todo.update (req.param 'todo'), (req.param 'completed') == 'yes')
  .done (t) ->
    res.redirect '/todos/' + t.id

_destroy = (req, res) ->
  (todo.remove req.param 'todo')
  .done ->
    res.redirect '/todos'


module.exports =
  index: _index
  new: _new
  create: _create
  show: _show
  edit: _edit
  update: _update
  destroy: _destroy
